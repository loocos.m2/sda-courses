package com.sda.oop.inheritance;

//absrtact class can not be initialized

public abstract class Vehicle {
    int tankCapacity;

    //abstract method - must be implemented
    public abstract int fillTank(int amount);

    public abstract void service();

    //regular method - the same for all vehicles
    public void drive() {
        System.out.println("driving...");
    }

}
