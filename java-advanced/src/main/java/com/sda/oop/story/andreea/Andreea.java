package com.sda.oop.story.andreea;

import com.sda.oop.story.skills.Developer;
import com.sda.oop.story.skills.Polyglot;
import com.sda.oop.story.women.FinancialSecurity;
import com.sda.oop.story.women.Love;
import com.sda.oop.story.women.Woman;

public class Andreea extends Woman implements Polyglot, Developer {

    private Pony pony;

    //composition
    public Andreea(Love love, FinancialSecurity financialSecurity) {
        super(love, financialSecurity);
    }

    @Override
    public void feel() {
        System.out.println("andreea feels");
    }

    @Override
    public void dream() {
        System.out.println("andreea dreams");
    }

    @Override
    public void relax() {
        System.out.println("andrea relaxes");
    }

    @Override
    public void speakEnglish() {
        System.out.println("hello");
    }

    @Override
    public void speakSpanish() {
        System.out.println("hola");
    }

    @Override
    public void writeCode() {
        System.out.println("andreea write code");
    }

    public void haveFun() {
        System.out.println("andreea is having fun");
    }


    public void getTanned() {
        System.out.println("andreea tanning");
    }


    public void getNailsDone() {
        System.out.println("andrea");
    }

    //aggregation
    public void pleaseGiveMeAPony(Pony pony) {
        if (pony == null) {
            System.out.println("Hey, you promised!");
        } else {
            this.pony = pony;
            System.out.println("Yey, now i can die happy! ");
            System.out.println("Hello, " + pony.getName());
        }
    }

    public void feedPony() {
        System.out.println("here you go mr " + this.pony.getName());
        this.pony.eat();
    }
}
