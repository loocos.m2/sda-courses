package com.sda.oop.story.andreea;

public class Pony {
    private String name;

    public Pony(String name) {
        this.name = name;
    }

    public void eat() {
        System.out.println("pony is eating");
    }

    public void sleep() {
        System.out.println("pony is sleeping!");
    }

    public String getName() {
        return name;
    }
}
