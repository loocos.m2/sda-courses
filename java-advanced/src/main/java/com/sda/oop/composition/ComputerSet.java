package com.sda.oop.composition;

//has a
//composition
public class ComputerSet {
    private Monitor monitor;
    private Mouse mouse;

    public ComputerSet(Monitor monitor, Mouse mouse) {
        this.monitor = monitor;
        this.mouse = mouse;
    }

    public void process() {
        monitor.turnOn();
        mouse.click();
        System.out.println("processing...");
    }

//    public Monitor getMonitor() {
//        return monitor;
//    }
//
//    public void setMonitor(Monitor monitor) {
//        this.monitor = monitor;
//    }
//
//    public Mouse getMouse() {
//        return mouse;
//    }
//
//    public void setMouse(Mouse mouse) {
//        this.mouse = mouse;
//    }
}
