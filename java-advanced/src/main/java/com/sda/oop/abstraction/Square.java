package com.sda.oop.abstraction;

public class Square extends Shape {

    double lenght;

    public Square(String color, double length) {
        super(color);
        System.out.println("square constructor");
        this.lenght = length;
    }

    @Override
    double area() {
        return 0;
    }

    @Override
    public String print() {
        return "color is " + super.color + " and length = " + this.lenght;
    }
}
