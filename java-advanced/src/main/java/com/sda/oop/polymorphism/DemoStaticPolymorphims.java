package com.sda.oop.polymorphism;

public class DemoStaticPolymorphims {
    public static void main(String[] args) {
        //reference of type Human = object of Boy
        Human boy = new Boy();

        //reference of type Human = object of Human
        Human human = new Human();


        boy.walk();//goes to human method
        human.walk();//goes to human method

        boy.run();//goes to human method
        human.run();//goes to human method
    }
}
