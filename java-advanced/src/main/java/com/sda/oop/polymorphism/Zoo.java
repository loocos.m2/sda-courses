package com.sda.oop.polymorphism;

public class Zoo {
    public void feed(Animal animal) {
        System.out.println("feed animal");
        animal.walk();
    }
}
