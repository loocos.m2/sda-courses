package com.sda.oop.polymorphism;

public class Boy extends Human {

    //not overrriding = static
    public static void walk() {
        System.out.println("boy walks");
    }

    //final can't ovveride

    //not overriding

    private void sing() {
        System.out.println("boy sings");
    }
}
