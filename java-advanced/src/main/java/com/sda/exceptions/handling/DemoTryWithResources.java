package com.sda.exceptions.handling;

import java.util.Scanner;

public class DemoTryWithResources {
    public static void main(String[] args) {
        // regularTryCatch();


        //exista errori si excepti din errori nu-ti poti reveni din errori da
        tryWithResources();

    }

    private static void tryWithResources() {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("wow you typed " + scanner.next() + " you so smart!");
        } catch (Exception e) {
            //handles exception
        }
        //no neeed to close scanner in finnaly
    }

    private static void regularTryCatch() {
        //read from user input
        Scanner scanner = null;
        try {
            scanner = new Scanner(System.in);
            System.out.println("wow you typed " + scanner.next() + " you so smart!");
        } catch (Exception e) {
            //handle exception
        } finally {
            //cleanuo
            if (scanner != null) {
                scanner.close();
            }
        }
        System.out.println(scanner.next());
    }
}
