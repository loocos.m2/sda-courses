package com.sda.exceptions.handling;

public class DemoThrows {
    public static void main(String[] args) throws Exception {
        //define elements
        int[] someIntArray = {3, 4, 2};

        //cheked exception
        try {
            printArrayElements(someIntArray, 3);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            ;
        }

        //printArrayElements(someIntArray,3);

    }

    //print array elements
    public static void printArrayElements(int[] array, int index) throws Exception {
        if (index < 0 || index >= array.length) {
            throw new Exception("incorect arguments");
        }
        System.out.println(array[index]);
    }
}
