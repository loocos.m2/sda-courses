package com.sda.exceptions.handling;

public class DemoTryCatch {
    public static void main(String[] args) {
        //   tryCatch();

        //muliple catch
        int[] intArray = {8, 6, 4};
        int index = 2;
        int y = 0;

//        int result = multipleCatch(intArray,index,y);
//        System.out.println(result);

        //catch with multiple params exception

        catchWithMultipleParams(intArray, index, y);

    }

    private static int catchWithMultipleParams(int[] array, int index, int divisor) {
        int result = 0;
        try {
            result = array[index] / divisor;
            //type | type ... name
        } catch (ArithmeticException | ArrayIndexOutOfBoundsException e) {
            System.out.println("Arithmethic exception or array out of bounds");
        }
        return result;
    }

    private static int multipleCatch(int[] array, int index, int divisor) {
        int x = 0;
        try {
            //variable scope
            x = array[index] / divisor;
        } catch (ArithmeticException e) {
            System.out.println("Arithmethic exception");
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("out of bounds");
        } catch (Exception e) {
            System.out.println("other exceptions");
        }
        return x;
    }

    private static void tryCatch() {
        int y = 2;
        //try catch

        try {
            int x = 5 / y;
            //catch (Type name)
        } catch (Exception e) {
            System.out.println("exception is caught and handled");
        } finally {
            System.out.println("this is printed no matter wath");
        }
    }
}
