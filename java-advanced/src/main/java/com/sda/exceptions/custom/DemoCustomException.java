package com.sda.exceptions.custom;

public class DemoCustomException {
    public static void main(String[] args) {

        try {
            Car car = new Car();
            car.increaseSpeed();
            car.increaseSpeed();
            car.increaseSpeed();
            car.increaseSpeed();
            car.increaseSpeed();
            car.increaseSpeed();
        } catch (CarCrashException e) {
            System.out.println("please decrease speed!");
        }
    }
}
