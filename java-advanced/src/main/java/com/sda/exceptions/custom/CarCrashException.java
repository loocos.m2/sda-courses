package com.sda.exceptions.custom;

//1. extend exception
public class CarCrashException extends Exception {

    //2.add details

    //override constructor
    public CarCrashException(String message) {
        super(message);
    }

    //custom consctructor
    public CarCrashException(Car car) {
        super("car " + car + " has crashed!");
    }

}
