package com.sda.collections.generics;

public class DemoGenerics {
    public static void main(String[] args) {
        ToyCar toyCar = new ToyCar();
        ToyPlane toyPlane = new ToyPlane();
        RegularBox boxWithToy = new RegularBox(toyCar);

        GenericBox<ToyCar> boxWithCars = new GenericBox<>(toyCar);
        boxWithCars.setItem(toyCar);

        GenericBox<ToyPlane> boxWithPlanes = new GenericBox<>(toyPlane);
        boxWithPlanes.setItem(toyPlane);
    }
}
