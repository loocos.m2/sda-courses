package com.sda.collections.list;

public class InvaliRatingException extends RuntimeException {

    public InvaliRatingException(String message) {
        super(message);
    }
}
