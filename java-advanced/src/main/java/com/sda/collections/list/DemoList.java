package com.sda.collections.list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DemoList {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();

        //add elements
        list.add("one");
        list.add("two");
        list.add("three");

        //insert element at position one
        list.add(1, "four");

        //read
        //read element at index 2
        list.get(2);


        //counts elements
        System.out.println(list.size());

        //update
        //change an item at a given index
        list.set(1, "five");

        //iterate elements
        list.iterator();

        //delete
        if (list.contains("one")) {
            list.remove("one");
        }

        //iterate elements
        iterateUsingForEach(list);
        iterateUsingFor(list);
        iterateUsingIterator(list);
    }

    private static void iterateUsingIterator(List<String> list) {
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    private static void iterateUsingFor(List<String> list) {
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }

    private static void iterateUsingForEach(List<String> list) {
        for (String item : list) {
            System.out.println(item);
        }
    }


}

