package com.sda.enums;

public enum Role {

    FIGHTER("sword ready"),  //elementele se definesc cu litere mari in enum si cu virgula dua
    ARCHER("arrows ready"),
    ASSASSIN("poison ready"); //obtii descrierera print=un getter

    private String description;  //variabila pentru a salva descrierea

    Role(String description) {
        this.description = description;
    }

    //print all elements in enum
    public void printEnum() {
        //for (type element : collection){}
        for (Role role : Role.values()) {    //Role.values face array din valorile enumului. - get values of enums
            System.out.println(role.name()); //printeaza numele FIGHTER
            System.out.println(role.description); //printeaza descrierea sword ready
        }
    }

    public String getDescription() {   //getter pentru a lua descrierea
        return description;
    }
}
