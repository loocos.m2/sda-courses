package com.sda.enums;

public class Hero {

    private int hp;
    private Role role;  //Hero has a Role

    public Hero(int hp, Role role) {
        this.hp = hp;
        this.role = role;
    }

    public Role getRole() {
        return role;
    }
}
