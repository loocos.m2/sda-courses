package com.sda.testing.homework1;

import com.sda.testing.junit4.Calculator;
import org.junit.Test;

public class HomeworkTest {

    @Test
    public void givenTwoVariables_whenCalculator_thenSubstract() {
        //given
        Calculator calculator = new Calculator();

        //when
        int actualSum = calculator.substract(3, 1);
        int expectedSum = 2;

        //then
        assert actualSum == expectedSum;
    }

    @Test
    public void givenTwoVariables_whenCalculator_thenMultiply() {
        //given
        Calculator calculator = new Calculator();

        //when
        int actualSum = calculator.multiply(2, 2);
        int expectedSum = 4;

        //then
        assert actualSum == expectedSum;
    }

    @Test
    public void givenAge_whenIsTeenagerMethod_thenReturnTrueOrFalse() {
        //given
        Person person = new Person();

        //when
        boolean actualAge = person.isTeenager(11);

        //then
        assert actualAge;
    }
}
