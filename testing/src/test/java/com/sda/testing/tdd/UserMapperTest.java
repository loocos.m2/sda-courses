package com.sda.testing.tdd;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class UserMapperTest {

    //user with all details -> convert to user with some details
    @Test
    public void givenUser_whenConvert_thenReturnUserDetails() {

        //given

        UserMapper userMapper = new UserMapper();
        User user = new User("Jon", "secret", "jonsnow@email");

        //when
        UserDetails actual = userMapper.convert(user);
        UserDetails expected = new UserDetails("Jon", "jonsnow@email");

        //then
        assertThat(actual).isEqualTo(expected);

    }
}
