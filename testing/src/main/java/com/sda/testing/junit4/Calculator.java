package com.sda.testing.junit4;

public class Calculator {

    public int sum(int firstOperand, int secondOperand) {
        return firstOperand + secondOperand;
    }

    // implement division
    public int divide(int a, int b) {
        if (b == 0) {
            throw new ArithmeticException("/ by zero");
        }
        return a / b;
    }

    public int substract(int firstOperand, int secondOperand) {
        return firstOperand - secondOperand;
    }

    public int multiply(int firstOperand, int secondOperand) {
        return firstOperand * secondOperand;
    }
}
