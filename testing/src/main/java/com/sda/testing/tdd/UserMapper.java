package com.sda.testing.tdd;

public class UserMapper {
    public UserDetails convert(User user) {
        //copy all fields from user to an object user details
        UserDetails userDetails = new UserDetails(user.name, user.email);
        //UserDetails.name = user.name;
        //UserDetails.email = user.email;

        return userDetails;
    }
}
